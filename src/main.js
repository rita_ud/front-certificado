import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import store from './store'



Vue.config.productionTip = false

import Login from '@/components/Login'
import axios from 'axios'

axios.defaults.baseURL = "http://172.16.5.187/api/certificados/"

Vue.component("Login", Login);

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
