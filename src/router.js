import Vue from 'vue'
// import Router from 'vue-router'
import VueRouter from 'vue-router';
import { ACCESS_TOKEN, REFRESH_TOKEN } from '@/services/auth';

// Vue.use(Router)

// Importar páginas
import Portada from '@/pages/portada/'
import Main from '@/pages/main/'
import Plantilla from '@/pages/plantilla/'
import File from '@/pages/file/'
import Login from '@/pages/login/'
//import Course from '@/pages/course/'

Vue.use(VueRouter);

const PUBLIC_PATHS = ['/', '/login'];

const routes = [
  {
    path: '/',
    name: 'portada',
    component: Portada
  },{
    path: '/main',
    name: 'main',
    component: Main
  },{
    path: '/plantilla',
    name: 'plantilla',
    component: Plantilla
  },{
    path: '/file',
    name: 'file',
    component: File
  },{
    path: '/login',
    name: 'login',
    component: Login
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

const unAuthenticatedAndPrivatePage = (path) => (!PUBLIC_PATHS.includes(path)
    && !(ACCESS_TOKEN in window.localStorage)
    && !(REFRESH_TOKEN in window.localStorage));

router.beforeEach((to, from, next) => {
  if (unAuthenticatedAndPrivatePage(to.path)) {
    next(`/login?next=${to.path}`);
  } else {
    next();
  }
});

export default router;
// export default new Router({
//   routes: [
//       {
//         path: '/',
//         name: 'portada',
//         component: Portada  
//       },{
//         path: '/main',
//         name: 'main',
//         component: Main
//       },{
//         path: '/plantilla',
//         name: 'plantilla',
//         component: Plantilla
//       },{
//         path: '/file',
//         name: 'file',
//         component: File
//       },{
//         path: '/login',
//         name: 'login',
//         component: Login
//       }


//       /*,{
//         path: '/course',
//         name: 'course',
//         component: Course
//       }*/
//   ]
  
// });
